var mongoose  = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({

  firstname: String,
  lastname: String,
  nickname: String,
  born: Number,
  died: Number,
  img: String,
  career: String,
  bio: String

});

const Pres = mongoose.model('President', schema);

module.exports = Pres;
