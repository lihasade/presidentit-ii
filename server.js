const express = require('express');
const Database = require('./database.js');
const Pres = require('./model/president.js');

const connection = new Database();
const app = express();
const port = 8080;

var cors = require('cors');

// CORS-middlewaren käyttöönotto
app.use(cors());
// Sallitaan pääsy selaimen tarvitsemiin tiedostoihin
app.use(express.static(__dirname+'/client')); 

app.get('/api/v1/president', (req, res, next) => {
  console.log("GET /api/v1/president");
    Pres.find({}).then(function(presidents){
        res.send(presidents);
    }).catch(next);
});

app.listen(port, '0.0.0.0', () => console.log(`Example app listening on port ${port}!`));
